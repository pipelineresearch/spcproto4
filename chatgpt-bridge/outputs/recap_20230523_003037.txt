Configuration utilisée :
project_name="node-graph-proto2"
src_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/src"
output_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/chatgpt-bridge/outputs"
# files_to_check=("Node.tsx" "Port.tsx" "nodeActions.ts" "nodesReducer.ts" "nodeTypes.ts")
files_to_check=("connectionActions.ts" "connectionTypes.ts" "connectionsReducer.ts" "node.tsx" "AddConnectionsButton.tsx") 
//////////////////////////////////////////////////////////////////////////
 
Nom du Projet: node-graph-proto2
Date / Heure: Tue, May 23, 2023 12:30:37 AM
Arborescence :
App.css
App.tsx
components
-Connection
--Connection.css
--Connection.tsx
--index.ts
-Lib
--AddConnectionsButton
---AddConnectionsButton.css
---AddConnectionsButton.tsx
--AddNodesButton
---AddNodesButton.css
---AddNodesButton.tsx
-Node
--index.ts
--Node.config.ts
--Node.tsx
-NodeArea
--index.ts
--NodeArea.css
--NodeArea.tsx
-Port
--index.ts
--Port.css
--Port.tsx
index.css
index.tsx
redux
-actions
--connectionActions.ts
--index.ts
--nodeActions.ts
--uiActions.ts
-NodesSlices.ts
-reducers
--connectionsReducer.ts
--nodesReducer_bak.ts
--uiReducer.ts
-reducers.ts
-store.ts
shema_state.js
types
-connectionTypes.ts
-index.ts
-nodeTypes.ts
-uiTypes.ts
vite-end.d.ts
Contenus des fichiers: connectionActions.ts connectionTypes.ts connectionsReducer.ts node.tsx AddConnectionsButton.tsx
////// AddConnectionsButton.tsx /////////
import React from 'react';
import { useDispatch } from 'react-redux';
import { addConnection, addNode } from '../../../redux/actions';
import './AddConnectionsButton.css'



const AddNodesButton: React.FC = () => {
    const dispatch = useDispatch();
  
    const handleClick = () => {
      dispatch(addConnection("node1", "port2", "node2", "port3"));
    };
  
    return (
      <button className="button" onClick={handleClick}>Add Connection</button>
    );
  };

export default AddNodesButton;////// connectionActions.ts /////////
import { AddConnectionAction, RemoveConnectionAction } from '../../types';

export const ADD_CONNECTION = 'ADD_CONNECTION';
export const REMOVE_CONNECTION = 'REMOVE_CONNECTION';

export const addConnection = (sourceNodeId: string, sourceOutputId: string, targetNodeId: string, targetInputId: string): AddConnectionAction => {
  return {
    type: ADD_CONNECTION,
    payload: {
      id: 'conn_' + Date.now(),  // or any other unique id generator
      source: { nodeId: sourceNodeId, outputPortId: sourceOutputId },
      target: { nodeId: targetNodeId, inputPortId: targetInputId },
    },
  };
};

export const removeConnection = (connectionId: string): RemoveConnectionAction => {
  return {
    type: REMOVE_CONNECTION,
    payload: {
      id: connectionId,
    },
  };
};////// connectionsReducer.ts /////////
import { Connections, ConnectionAction } from '../../types';
import { ADD_CONNECTION, REMOVE_CONNECTION } from '../actions';

const initialState: Connections = {};

const connectionReducer = (state = initialState, action: ConnectionAction): Connections => {
  switch (action.type) {
    case ADD_CONNECTION:
      return {
        ...state,
        [action.payload.id]: action.payload,
      };
    case REMOVE_CONNECTION:
      // à ecrire
      return state;
    default:
      return state;
  }
};

export default connectionReducer;


////// connectionTypes.ts /////////
import { Action } from 'redux';
import {ADD_CONNECTION,  REMOVE_CONNECTION} from '../redux/actions';

export interface Connection {
  id: string;
  source: {
    nodeId: string;
    outputPortId: string;
  };
  target: {
    nodeId: string;
    inputPortId: string;
  };
}

export interface Connections {
  [id: string]: Connection;
}

export interface AddConnectionAction extends Action<typeof ADD_CONNECTION> {
  payload: Connection;
}

export interface RemoveConnectionAction extends Action<typeof REMOVE_CONNECTION> {
  payload: {
    id: string;
  };
}

export type ConnectionAction = AddConnectionAction | RemoveConnectionAction;
