Configuration utilisée :
project_name="node-graph-proto2"
src_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/src"
output_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/chatgpt-bridge/outputs"
# files_to_check=("Node.tsx" "Port.tsx" "nodeActions.ts" "nodesReducer.ts" "nodeTypes.ts")
files_to_check=("nodeSlice.ts" "node.tsx" ) 
//////////////////////////////////////////////////////////////////////////
 
Nom du Projet: node-graph-proto2
Date / Heure: Tue, May 23, 2023 12:54:09 AM
Arborescence :
App.css
App.tsx
components
-Connection
--Connection.css
--Connection.tsx
--index.ts
-Lib
--AddConnectionsButton
---AddConnectionsButton.css
---AddConnectionsButton.tsx
--AddNodesButton
---AddNodesButton.css
---AddNodesButton.tsx
-Node
--index.ts
--Node.config.ts
--Node.tsx
-NodeArea
--index.ts
--NodeArea.css
--NodeArea.tsx
-Port
--index.ts
--Port.css
--Port.tsx
index.css
index.tsx
redux
-actions
--connectionActions.ts
--index.ts
--nodeActions.ts
--uiActions.ts
-reducers
--connectionsReducer.ts
--nodesReducer_bak.ts
--uiReducer.ts
-reducers.ts
-slices
--connectionSlice.ts
--nodeSlice.ts
--uiSlice.ts
-store.ts
shema_state.js
types
-connectionTypes.ts
-index.ts
-nodeTypes.ts
-uiTypes.ts
vite-end.d.ts
Contenus des fichiers: nodeSlice.ts node.tsx
////// nodeSlice.ts /////////
// src/redux/nodesSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Node } from '../../types';
import { nodeConfig } from '../../components/Node/Node.config';

// Define the initial state
const initialState: Record<string, Node> = {};

// Define the slice
const nodeSlice = createSlice({
  name: 'nodes',
  initialState,
  reducers: {
    addNode: (state, action: PayloadAction<Node>) => {
      const node = action.payload;
      const newNode: Node = {
        ...node,
        inputs: node.inputs.map((input, index) => ({
          ...input,
          x: nodeConfig.inputsMarginLeft,
          y: nodeConfig.headerHeight + index * nodeConfig.spacingVerticalBetweenPorts,
        })),
        outputs: node.outputs.map((output, index) => ({
          ...output,
          x: nodeConfig.nodeWidth + nodeConfig.outputsMarginRight,
          y: nodeConfig.headerHeight + index * nodeConfig.spacingVerticalBetweenPorts,
        })),
      };
      state[node.id] = newNode;
    },
    updateNode: (state, action: PayloadAction<Node>) => {
      const node = action.payload;
      state[node.id] = node;
    },
    moveNode: (state, action: PayloadAction<{ nodeId: string, x: number, y: number }>) => {
      const { nodeId, x, y } = action.payload;
      state[nodeId].x = x;
      state[nodeId].y = y;
    },
  },
});

// Export the actions
export const { addNode, updateNode, moveNode } = nodeSlice.actions;

// Export the reducer
export default nodeSlice.reducer;