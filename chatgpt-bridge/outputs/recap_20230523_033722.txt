Configuration utilisée :
project_name="node-graph-proto2"
src_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/src"
output_dir="E:/PROJECTS/6-AI/nodeGraphChatGPT/SPCproto4/chatgpt-bridge/outputs"
# files_to_check=("Node.tsx" "Port.tsx" "nodeActions.ts" "nodesReducer.ts" "nodeTypes.ts")
files_to_check=("OutputPort.tsx" "InputPort.tsx" ) 
//////////////////////////////////////////////////////////////////////////
 
Nom du Projet: node-graph-proto2
Date / Heure: Tue, May 23, 2023  3:37:22 AM
Arborescence :
App.css
App.tsx
components
-Connection
--Connection.css
--Connection.tsx
--index.ts
-InputPort
--index.ts
--InputPort.tsx
-Lib
--AddConnectionsButton
---AddConnectionsButton.css
---AddConnectionsButton.tsx
--AddNodesButton
---AddNodesButton.css
---AddNodesButton.tsx
-Node
--index.ts
--Node.config.ts
--Node.tsx
-NodeArea
--index.ts
--NodeArea.css
--NodeArea.tsx
-OutputPort
--index.ts
--OutputPort.tsx
-Port
--index.ts
--Port.tsx
index.css
index.tsx
redux
-slices
--connectionSlice.ts
--nodeSlice.ts
--uiSlice.ts
-store.ts
shema_state.js
types
-connectionTypes.ts
-index.ts
-nodeTypes.ts
-uiTypes.ts
vite-end.d.ts
Contenus des fichiers: OutputPort.tsx InputPort.tsx
////// InputPort.tsx /////////
import { useDrop } from "react-dnd";
import { styled } from '@linaria/react';
import { nodeConfig } from '../Node/Node.config';

interface InputPortProps {
  nodeId: string;
  portId: string;
  portName: string;
  x: number;
  y: number;
  nodeConfig: typeof nodeConfig;
}

const InputPort: React.FC<InputPortProps> = ({ nodeId, portId, portName, x, y, nodeConfig }) => {
  const [, dropInput] = useDrop({
    accept: "connection",
    drop: (item: { sourceNodeId: string; sourcePortId: string }) => {
      // Créer une connexion
      console.log(
        `Connection created: Source - Node: ${item.sourceNodeId} Port: ${item.sourcePortId} Target - Node: ${nodeId} Port: ${portId}`
      );
    }
  });

  return (
    <InputDOM ref={dropInput} x={x} y={y}>
      <AnchorInputPortDOM
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        AnchorInputColor={nodeConfig.AnchorInputColor}
        border={nodeConfig.border}
      />
      <InputLabelDOM
        nodeWidth={nodeConfig.nodeWidth}
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
      >
        {portName}
      </InputLabelDOM>
    </InputDOM>
  );
};

export default InputPort;

const InputDOM = styled.div<{
    x: number;
    y: number;
  }>`
    position: absolute;
    top: ${({ y }) =>`${y}px`};
    left: ${({ x }) => `${x}px`};
  
  `;
  
  const AnchorInputPortDOM = styled.div<{ 
    radiusAnchorPort: number ;
    AnchorInputColor: string ;
    border: string; 
  }>`
  
    position: absolute;
    top: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
    left: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
    width: ${props =>`${props.radiusAnchorPort}px`};
    height:  ${props =>`${props.radiusAnchorPort}px`};
    border-radius: 100%;
    background-color: ${props =>`${props.AnchorInputColor}`};
    outline: ${props => `${props.border}`};
  `;
  
  const InputLabelDOM = styled.div<{ nodeWidth: number ; radiusAnchorPort: number }>`
    position: absolute;
    text-align: left;
    margin-left: 15px;
    width: ${props => `${props.nodeWidth}px`};
    top:${props =>`${props.radiusAnchorPort / 2 *-1 }px`};
    left:0;
  
  `;////// OutputPort.tsx /////////
import { useDrag, useDrop } from "react-dnd";
import { styled } from '@linaria/react';

import { nodeConfig } from '../Node/Node.config';

interface OutputPortProps {
  nodeId: string;
  portId: string;
  portName: string;
  x: number;
  y: number;
  nodeConfig: typeof nodeConfig;
}

const OutputPort: React.FC<OutputPortProps> = ({ nodeId, portId, portName, x, y, nodeConfig }) => {
  const [{ isDragging }, dragOutput] = useDrag({
    type: "connection",
    item: {
      sourceNodeId: nodeId,
      sourcePortId: portId
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })
  });

  return (
    <OutputDOM ref={dragOutput} x={x} y={y}>
      <AnchorOutputPortDOM
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        AnchorOutputColor={nodeConfig.AnchorOutputColor}
        border={nodeConfig.border}
      />
      <OutputLabelDOM
        nodeWidth={nodeConfig.nodeWidth}
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
      >
        {portName}
      </OutputLabelDOM>
    </OutputDOM>
  );
};


export default OutputPort;

const OutputLabelDOM = styled.div<{ nodeWidth: number ; radiusAnchorPort: number }>`
  position: absolute;
  text-align: right;
  margin-right: 15px;
  width: ${props => `${props.nodeWidth/2}px`};
  top: ${props =>`${props.radiusAnchorPort / 2 *-1 }px`};
  right:0;
  pointer-events: none;
`
const OutputDOM = styled.div<{
  x: number;
  y: number;
}>`
  position: absolute;
  top: ${({ y }) =>`${y}px`};
  right: ${({ x }) => `${x}px`};

`;

const AnchorOutputPortDOM = styled.div<{ 
  radiusAnchorPort: number ;
  AnchorOutputColor: string ;
  border: string;
}>`

  position: absolute;
  top: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
  left: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
  width: ${props =>`${props.radiusAnchorPort}px`};
  height:  ${props =>`${props.radiusAnchorPort}px`};
  border-radius: 100%;
  background-color: ${props =>`${props.AnchorOutputColor}`};
  outline: ${props => `${props.border}`};
`;