import express from 'express';
import cors from 'cors';
import { readdirSync, statSync, readFileSync } from 'fs';
import { join, extname, basename } from 'path';

const app = express();
app.use(cors());

function getFiles(dirPath, fileList = []) {
    const files = readdirSync(dirPath);

    for (const file of files) {
        const filePath = join(dirPath, file);
        const stat = statSync(filePath);

        if (stat.isDirectory()) {
            getFiles(filePath, fileList);
        } else if (stat.isFile()) {
            const ext = extname(filePath);
            if (ext === '.json') { // Nous vérifions si le fichier est un .json
                const content = JSON.parse(readFileSync(filePath, 'utf8'));
                const name = basename(file, ext);
                fileList.push({
                    type: 'file',
                    name: name, // Nous enregistrons uniquement le nom du fichier sans l'extension
                    extension: ext.slice(1), // Nous enregistrons l'extension sans le point
                    content: content,
                });
            }
        }
    }

    return fileList;
}

app.get('/files', (req, res) => {
    const files = getFiles('./Lib');
    res.json(files);
});

app.listen(3001, () => {
    console.log('Server running on port 3001');
});