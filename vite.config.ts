import { defineConfig } from 'vite';
import react from "@vitejs/plugin-react-swc";
import tsconfigPaths from 'vite-tsconfig-paths';
import linaria from '@linaria/rollup';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    tsconfigPaths(),
    linaria({
      exclude: '**/.vite/**',
    }),
  ],
  define: {
    'process.env': {},
  },
});