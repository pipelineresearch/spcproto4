import { useDispatch } from 'react-redux';
import { createWindow } from '../../redux/slices/uiSlice';

const addUiButton: React.FC = () => {
  const dispatch = useDispatch();

  const handleButtonClick = () => {
    dispatch(createWindow({
      id: 'newWindowId',
      title: 'New Window',
      component: 'NodeArea',
    }));
  };

  return (
    <button onClick={handleButtonClick}>Create new window</button>
  );
};

export default addUiButton;