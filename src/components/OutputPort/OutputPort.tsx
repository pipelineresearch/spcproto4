import React, { useEffect } from 'react';
import { useDrag } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { styled } from '@linaria/react';
import { v4 as uuidv4 } from 'uuid';
import { nodeConfig } from '../Node/Node.config';

interface OutputPortProps {
  nodeId: string;
  portId: string;
  portName: string;
  x: number;
  y: number;
  nodeConfig: typeof nodeConfig;
}

const ItemTypes = {
  PORT: "port",
};

const OutputPort: React.FC<OutputPortProps> = ({ nodeId, portId, portName, x, y, nodeConfig }) => {
  const [{ isDragging }, drag, preview] = useDrag({
    type: ItemTypes.PORT,
    item: () => {
      const newId = uuidv4();
      const newConnection = {
        id: newId,
        source: {
          nodeId,
          outputPortId: portId,
        },
        target: {
          nodeId: "",
          inputPortId: "",
        },
      };
      console.log(newConnection);
      return newConnection;
    },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  return (
    <OutputDOM isDragging={isDragging} ref={drag} x={x} y={y}>
      <AnchorOutputPortDOM
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        AnchorOutputColor={nodeConfig.AnchorOutputColor}
        border={nodeConfig.border}
      />
      <OutputLabelDOM
        nodeWidth={nodeConfig.nodeWidth}
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        fontSizePorts={nodeConfig.fontSizePorts}
      >
        {portName}
      </OutputLabelDOM>
    </OutputDOM>
  );
};


export default OutputPort;

const OutputDOM = styled.div<{
  x: number;
  y: number;
  isDragging: boolean;
}>`
  position: absolute;
  top: ${({ y }) =>`${y+5}px`};
  right: ${({ x }) => `${x}px`};
  cursor: ${({ isDragging }) => (isDragging ? 'grabbing' : 'grab')};
`;

const OutputLabelDOM = styled.div<{ 
  nodeWidth: number ; 
  radiusAnchorPort: number ;
  fontSizePorts:number ; 
}>`
  position: absolute;
  text-align: right;
  margin-right: 10px;
  width: ${props => `${props.nodeWidth/2}px`};
  top: ${props =>`${props.radiusAnchorPort / 2 * 1.3 * -1 }px`};
  right:0;
  pointer-events: none;
  font-size: ${props => `${props.fontSizePorts}px`};
`

const AnchorOutputPortDOM = styled.div<{ 
  radiusAnchorPort: number ;
  AnchorOutputColor: string ;
  border: string;
}>`
  position: absolute;
  top: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
  left: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
  width: ${props =>`${props.radiusAnchorPort}px`};
  height:  ${props =>`${props.radiusAnchorPort}px`};
  border-radius: 100%;
  background-color: ${props =>`${props.AnchorOutputColor}`};
  outline: ${props => `${props.border}`};
`;
