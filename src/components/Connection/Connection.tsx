// Connection.tsx
import React from 'react';
import { Connections as ConnectionsType} from '../../types'; // Import your types
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';

import './Connection.css'


const Connection: React.FC<ConnectionsType> = ({ connection }) => {
  const nodes = useSelector((state: RootState) => state.nodes);
  const widgets = useSelector((state: RootState) => state.ui.widgets);

  const sourceNode = nodes[connection.source.nodeId];
  const targetNode = nodes[connection.target.nodeId];
  const targetWidget = widgets.find(widget => widget.id === connection.target.nodeId);


  const sourcePort = sourceNode.outputs.find(output => output.portId === connection.source.outputPortId) ;
  const targetPort = targetNode?.inputs.find(input => input.portId === connection.target.inputPortId) ? 
  targetNode?.inputs.find(input => input.portId === connection.target.inputPortId)  : targetWidget?.inputs.find(input => input.portId === connection.target.inputPortId) ;

  if (!sourcePort || !targetPort || !sourceNode?.x || !sourceNode?.y ) {
    return null; // Or some other default value or error handling
  }

let targetX = targetNode?.x ;
let targetY = targetNode?.y ;
  if (!targetX || !targetY) { //fix très temporaire et très moche pour les widgets
    targetX = targetWidget?.x +1180;
    targetY = targetWidget?.y +10;
  }

  

  const startX = sourceNode.x + sourcePort.x;
  const startY = sourceNode.y + sourcePort.y -30;
  let endX = targetX + targetPort.x;
  let endY = targetY + targetPort.y -30;  // toujours le même fix de merde pour compenser la barre de drag des fenetre react-grid-layout
  if (!targetX || !targetY) {
    endX = targetX ;
    endY = targetY -30; // toujours le même fix de merde pour compenser la barre de drag des fenetre react-grid-layout
  }
  const controlPointX1 = startX + 100;
  const controlPointY1 = startY;

  const controlPointX2 = endX - 100;
  const controlPointY2 = endY;

  const path = `M ${startX},${startY} C ${controlPointX1},${controlPointY1} ${controlPointX2},${controlPointY2} ${endX},${endY}`;

  return (
      <path d={path} strokeWidth="3px" stroke="brown" fill="transparent"  />
  );
};

export default Connection;