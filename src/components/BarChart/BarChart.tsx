import React from 'react';
import c3 from 'c3';

import './BarChart.css';

type BarChartProps = {
  datasX: number[];
  datasY: number[];
  height: number;
  width: number;
};


const BarChart: React.FC <BarChartProps> = ({
  datasX = [30, 200, 100, 400, 150, 250],
  datasY = [50, 20, 10, 40, 15, 25],
  height = 300,
  width = 300,
}) => {  React.useEffect(() => {
    const chart = c3.generate({
      bindto: '#chart',
      data: {
        columns: [
          ['data1', ...datasX],
          ['data2', ...datasY],
        ],
        type: 'bar',
      },
    });
    chart.resize({height, width});
  }, []);



  return <div id="chart" />;
};

export default BarChart;