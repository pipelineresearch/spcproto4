// NodeCard.tsx
import React, { useRef } from 'react';
import { useDrag, DragSourceMonitor } from 'react-dnd';
import { useDispatch } from 'react-redux';
import { Node as NodeType , Widget as widgetType} from '../../types';
import { v4 as uuidv4 } from 'uuid';
import { addNode } from '../../redux/slices/nodeSlice';
import { addWidget } from '../../redux/slices/uiSlice';
import { styled } from '@linaria/react';

interface DropResult {
    x: number;
    y: number;
    id: string; // id of NodeArea where NodeCard was dropped
    // Ajoutez ici toutes les autres propriétés que vous attendez du résultat du dépôt
  }
  
  
interface NodeCardProps extends NodeType {
    id: string;
    parent: string;
    category: string;
}


const NodeCard: React.FC<NodeCardProps> = (props) => {
    const dispatch = useDispatch();
    const ref = useRef<HTMLDivElement>(null);
    const offset = useRef<{ offsetX: number, offsetY: number }>({ offsetX: 0, offsetY: 0 });


    const [{ isDragging }, drag] = useDrag(() => ({
      type: 'card',
      item: { ...props},
      options: { dropEffect: 'move' },
      end: (item: NodeType | widgetType | undefined, monitor: DragSourceMonitor) => {
        const dropResult = monitor.getDropResult<DropResult>();
      
        // Clear the drag offset at the end of the drag
        offset.current = { offsetX: 0, offsetY: 0 };
      
        if (item && dropResult) {
          const { offsetX, offsetY } = offset.current;
          if (typeof offsetX === 'number' && typeof offsetY === 'number') {
            switch (item.category) {
              case 'node': {
                if ('inputs' in item && 'outputs' in item) {
                  const newNode = {
                    name: item.name,
                    category: item.category,
                    inputs: item.inputs,
                    outputs: item.outputs,
                    id: uuidv4(),
                    parent: dropResult.id,
                    x: dropResult.x - offsetX,
                    y: dropResult.y - offsetY,
                  };
                  dispatch(addNode(newNode));
                }
                break;
              }
              case 'widget': {
                if ('component' in item) {
                  const newWidget = {
                    id: uuidv4(),
                    name: item.name,
                    component: item.component,
                    category: item.category,
                    isNodeAreaActive: false,
                    x: dropResult.x - offsetX,
                    y: dropResult.y - offsetY,
                    w: item.w, // width of the grid item
                    h: item.h, // height of the grid item
                    inputs: item.inputs,
                  };
                  dispatch(addWidget(newWidget)); // You will need to define this action in your widgetSlice
                }
                break;
              }
              default:
                break;
            }
          }
        }
      },
    }),[""] );
  
    // drag(ref);
  

    return (
        <NodeCardDOM 
            ref={drag}
            style={{
               backgroundColor: props.category === "node" ? "#ffb340" : "#03816d" 
            }}
        >
            <CardLabel style={{ color: props.category === "node" ? "#0f0a06" : "#0f0a06" }}>{props.name}</CardLabel>
        </NodeCardDOM>
    )
}

export default NodeCard;

const NodeCardDOM = styled.div`
    display: flex;
    height: 35px;

    justify-content: center;
    align-items: center;
    border-radius: 30px;
    margin: 8px;
    color:black;

    `
const CardLabel = styled.div`
    font-size: 15px;
    font-weight: 500;
    color : #ffb340
    `