// NodeArea.tsx
import React, {useRef, useEffect} from 'react';
import { useDrop } from 'react-dnd';
import { useSelector , useDispatch} from 'react-redux';
import { RootState } from '../../redux/store'; 
import Node from '../Node';
import Connection from '../Connection';
import { styled } from '@linaria/react';
import { Connection as connectionType , Node as nodeType , Widget as WidgetType} from '../../types';
import { deleteNode } from '../../redux/slices/nodeSlice';
import { deselectNode } from '../../redux/slices/uiSlice';
import  InputWidgetPort  from '../InputWidgetPort'

interface NodeAreaProps {
  id: string;
  parentUI: WidgetType[];
  x: number;
  y: number;
}

interface NodesState {
  nodes: [nodeType];

}

const NodeArea: React.FC<NodeAreaProps> = ({ id , parentUI, x , y})  => {
  const dispatch = useDispatch();
  const selectedNodeId = useSelector((state: RootState) => state.ui.selectedNodeId);
  const nodes:NodesState = useSelector((state: RootState) => state.nodes);
  const connections:connectionType = useSelector((state: RootState) => state.connections);
  const nodeAreaRef = useRef<HTMLDivElement>(null);

  const [, drop] = useDrop({
    accept: ['card', 'node'],
    drop: (item, monitor) => {
      const currentCursorPosition = monitor.getClientOffset();
      if(currentCursorPosition && nodeAreaRef.current){
        const nodeAreaRect = nodeAreaRef.current?.getBoundingClientRect();
        const adjustedX = nodeAreaRect ? currentCursorPosition.x - nodeAreaRect.left : monitor.getClientOffset()?.x;
        const adjustedY = nodeAreaRect ? currentCursorPosition.y - nodeAreaRect.top +30 : monitor.getClientOffset()?.y; /// le 30 c'est juste un fix de merde pour compenser la barre de drag des fenetre react-grid-layout
        return {
          id,
          x: adjustedX,
          y: adjustedY,
        };
      }else{
        console.log("currentCursorPosition or nodeAreaRef.current is undefined")
        return undefined;
      }
    },
      collect: (monitor) => ({
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
  }),
  });

  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.key === 'Delete' && selectedNodeId) {
        dispatch(deleteNode(selectedNodeId));
        dispatch(deselectNode());
      }
    };

    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [dispatch, selectedNodeId]);

  console.log("x",x) 
  console.log("y",y) 

  return (
    <NodeAreaStyle ref={nodeAreaRef} id={id} >
      <DropArea ref={drop}>
        <svg style={{ position: "relative", width: "100%", height: "100%" }}>
          {connections && Object.values(connections).map((connection) => (
            connection.parentId === id && <Connection key={connection.id} connection={connection} parentX={x} parentY={y}/>
          ))}
        </svg>
        {nodes && Object.values(nodes).map((node) => (
          node.parent === id && <Node key={node.id} id={node.id} node={node} />
        ))}
        {parentUI && Object.values(parentUI.inputs).map((input,index) => (
          <InputWidgetPort 
          key={index}
          nodeId={id}
          portId={input.portId}
          portName={input.name}
          x={input.x}
          y={input.y}
          />
        ))}

      </DropArea>
    </NodeAreaStyle>
  );
};

export default NodeArea;


const DropArea = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    background-color: #f0f0f0;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    transition: all 0.3s ease;
    background-color: #333;
`

const NodeAreaStyle = styled.div`
    
    height: 100%;
    &:active, &:hover {
      cursor: move !important;
    }
`;