import React from 'react';
import { useDispatch } from 'react-redux';
import { useDrop} from "react-dnd";
import { v4 as uuidv4 } from "uuid";
import { styled } from '@linaria/react';
import { addConnection } from '../../redux/slices/connectionSlice';
import { nodeConfig } from '../Node/Node.config';
import {Connection as connectionTypes} from '../../types';

interface InputPortProps {
  nodeId: string;
  portId: string;
  portName: string;
  x: number;
  y: number;
  parentId: string;
  nodeConfig: typeof nodeConfig;
}

const ItemTypes = {
    PORT: "port",
  };

const InputPort: React.FC<InputPortProps> = ({ nodeId, portId, portName, x, y, parentId, nodeConfig }) => {
  const dispatch = useDispatch();
  
  const [, drop] = useDrop(() => ({
    accept: ItemTypes.PORT,
    drop: (item:connectionTypes , monitor) => {
      console.log(item);
      const didDrop = monitor.didDrop();
      if (didDrop) {
        return;
      }

      dispatch(
        addConnection({
          id: uuidv4(),
          parentId: parentId,
          source: {
            nodeId: item.source.nodeId,
            outputPortId: item.source.outputPortId,
          },
          target: {
            nodeId: nodeId,
            inputPortId: portId,
          },
        })
      );
    },
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  }));

  return (
    <InputDOM  x={x} y={y}>
      <AnchorInputPortDOM ref={drop}
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        AnchorInputColor={nodeConfig.AnchorInputColor}
        border={nodeConfig.border}
      />
      <InputLabelDOM
        nodeWidth={nodeConfig.nodeWidth}
        radiusAnchorPort={nodeConfig.radiusAnchorPort}
        fontSizePorts={nodeConfig.fontSizePorts}
      >
        {portName}
      </InputLabelDOM>
    </InputDOM>
  );
};

export default InputPort;

const InputDOM = styled.div<{
    x: number;
    y: number;
  }>`
    position: absolute;
    top: ${({ y }) =>`${y+5}px`};
    left: ${({ x }) => `${x}px`};
  
  `;
  
  const AnchorInputPortDOM = styled.div<{ 
    radiusAnchorPort: number ;
    AnchorInputColor: string ;
    border: string; 
  }>`
  
    position: absolute;
    top: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
    left: ${props =>`${props.radiusAnchorPort /2 * -1 }px`};
    width: ${props =>`${props.radiusAnchorPort}px`};
    height:  ${props =>`${props.radiusAnchorPort}px`};
    border-radius: 100%;
    background-color: ${props =>`${props.AnchorInputColor}`};
    outline: ${props => `${props.border}`};
  `;
  
  const InputLabelDOM = styled.div<{ 
    nodeWidth: number ; 
    radiusAnchorPort: number ; 
    fontSizePorts:number; 
  }>`
    position: absolute;
    text-align: left;
    margin-left: 10px;
    width: ${props => `${props.nodeWidth}px`};
    top:${props =>`${props.radiusAnchorPort / 2 *1.5 *-1 }px`};
    left:0;
    font-size: ${props => `${props.fontSizePorts}px`};
  
  `;