import React from 'react';
import { Widget as WidgetType } from "../../types"; 
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { Responsive, WidthProvider } from "react-grid-layout";
import Widget from "../Widget";

const ResponsiveGridLayout = WidthProvider(Responsive);


const WidgetsArea: React.FC = () => {

  const widgets:WidgetType[] = useSelector((state:RootState) => state.ui.widgets);


  const layouts = {
    lg: widgets.map((widget:WidgetType) => {
      return {i: widget.id, x: widget.x, y: widget.y, w: widget.w, h: widget.h};
    }),
  };

  const onDrop = (layout, layoutItem, _event) => {
    console.dir( event);
  };

  return (

    <ResponsiveGridLayout
      layouts={layouts}
      rowHeight={window.innerHeight / 540}
      cols={{ lg: 100, md: 80, sm: 60, xs: 40, xxs: 20 }}
      draggableHandle=".dragHandle"
      isResizable={true}
      breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 100}}
      // onDrop={(event) => onDrop({},{"test":"test"},event)}
      // measureBeforeMount={true}
      // isDroppable={true}
      
    >
      
      {widgets.map((widget:WidgetType) => (
        <div key={widget.id}  data-grid={{ isResizable: true }} onDrop={onDrop}>
          <Widget widget={widget} />
        </div>
      ))}
    </ResponsiveGridLayout>
  );
};

export default WidgetsArea;