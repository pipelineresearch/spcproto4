export interface NodeConfig {
    headerHeight: number;
    footerHeight: number;
    inputsMarginLeft: number;
    outputsMarginRight: number;
    spacingVerticalBetweenPorts: number;
    nodeWidth: number;
    borderRadius:string;
    radiusAnchorPort:number;
    border: string;
    backgroundColorNode: string;
    AnchorInputColor:string;
    AnchorOutputColor:string;
    fontSizeTile:number;
    fontSizePorts:number;
    
  }
  
  export const nodeConfig: NodeConfig = {
    headerHeight: 35,
    footerHeight: 5,
    inputsMarginLeft: 0,
    outputsMarginRight: 0,
    spacingVerticalBetweenPorts: 23,
    nodeWidth: 150,
    backgroundColorNode: "#555",
    border: "3px solid #555 ",
    borderRadius: "10px",
    radiusAnchorPort: 12 ,
    AnchorInputColor:"green",
    AnchorOutputColor:"blue",
    fontSizeTile:13,
    fontSizePorts:11,
  };
  