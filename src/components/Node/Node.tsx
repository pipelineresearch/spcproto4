// Node.tsx
import React, { useRef } from 'react';
import { useDrag, DragSourceMonitor } from "react-dnd";
import { useDispatch , useSelector} from 'react-redux';
import { styled } from '@linaria/react';

import { selectNode } from '../../redux/slices/uiSlice';
import { moveNode } from '../../redux/slices/nodeSlice'; 
import { AppDispatch , RootState} from '../../redux/store';
import { Node as NodeType } from '../../types'; 

import { nodeConfig } from './Node.config';
import InputPort from '../InputPort/InputPort';
import OutputPort from '../OutputPort/OutputPort'; 


interface NodeProps {
  id:string;
  node: NodeType;
}

interface DropResult {
  x: number;
  y: number;
  id: string
  // Ajoutez ici toutes les autres propriétés que vous attendez du résultat du dépôt
}

interface DragItem {
  id: string;
  node: NodeType;
}

const Node: React.FC<NodeProps> = ({ id, node }) => {
  const dispatch = useDispatch<AppDispatch>();
  const ref = useRef<HTMLDivElement>(null);
  const offset = useRef<{ offsetX: number, offsetY: number }>({ offsetX: 0, offsetY: 0 });
  const selected = useSelector((state: RootState) => 
    state.ui.selectedNodeId === id
  );
  
  const [{ isDragging }, drag] = useDrag<DragItem, object, { isDragging: boolean }>({
    type: 'node',
    item: { id , node },
    end: (item: DragItem | undefined, monitor: DragSourceMonitor) => {
      const dropResult = monitor.getDropResult<DropResult>();
      if (item && dropResult && offset.current) {
        const { offsetX, offsetY } = offset.current;
        if (typeof offsetX === 'number' && typeof offsetY === 'number') {
          
            dispatch(moveNode({ nodeId: item.id, x: dropResult.x - offsetX, y: dropResult.y - offsetY }));
            console.log("monitor.getDropResult()", monitor.getDropResult());
        
        } else {
          console.warn(`Offset values are not numbers: c'est chelou`);
        }
      }
    },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
});

  drag(ref);

  const handleClick = () => {
    dispatch(selectNode(id));
  };

  const handleMouseDown = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const nodeRect = ref.current?.getBoundingClientRect();
    if (nodeRect) {
      offset.current = {
        offsetX: event.clientX - nodeRect.left,
        offsetY: event.clientY - nodeRect.top
      };
      console.log(`Offset for node ${id} is set to`, offset.current); // Log the offset
    }
  };

  const maxPorts = Math.max(node.inputs.length, node.outputs.length);
  const nodeHeight = nodeConfig.headerHeight + maxPorts * (nodeConfig.spacingVerticalBetweenPorts) + nodeConfig.footerHeight;
  return (

    <NodeDOM
    ref={ref}
    x={node.x || 0}
    y={node.y || 0}
    nodeWidth={nodeConfig.nodeWidth}
    nodeHeight={nodeHeight}
    backgroundColorNode={nodeConfig.backgroundColorNode}
    border={nodeConfig.border}
    onMouseDown={e => handleMouseDown(e)}
    onClick={handleClick}
    isDragging={isDragging}
    className={selected ? 'selected' : ''}
  >
    <HeaderDOM fontSizeTile={nodeConfig.fontSizeTile}>{node.name}</HeaderDOM>
    {node.inputs.map((input, index) => (
      <InputPort
        key={input.name}
        nodeId={id}
        portId={input.portId}
        portName={input.name}
        x={nodeConfig.inputsMarginLeft}
        y={nodeConfig.headerHeight + (nodeConfig.spacingVerticalBetweenPorts * index)}
        parentId={node.parent}
        nodeConfig={nodeConfig}
      />
    ))}
  {node.outputs.map((output, index) => (
    <OutputPort
      key={output.name}
      nodeId={id}
      portId={output.portId}
      portName={output.name}
      x={nodeConfig.outputsMarginRight}
      y={nodeConfig.headerHeight + (nodeConfig.spacingVerticalBetweenPorts * index)}
      nodeConfig={nodeConfig}
    />
  ))}
    <FooterDOM 
    footerHeight= {nodeConfig.footerHeight}
    />
  </NodeDOM>
  );
};


const NodeDOM = styled.div<{
  x: number;
  y: number;
  nodeWidth: number;
  nodeHeight: number;
  backgroundColorNode: string;
  border: string;
  isDragging: boolean;
}>`
  position: absolute;
  opacity: ${props => props.isDragging ? "0" : "1"};
  top: ${props =>`${props.y}px`};
  left: ${props => `${props.x}px`};
  width: ${props => `${props.nodeWidth}px`};
  height: ${props => `${props.nodeHeight}px`};
  background-color: ${props => `${props.backgroundColorNode}`};
  outline: ${props => `${props.border}`};
  border-radius: 5px;

  &.selected {
    outline: 2px solid orange; // Exemple de style lorsqu'il est sélectionné
  }
  color: black;
`;

const HeaderDOM = styled.div<{
  fontSizeTile: number;
}>`
  display: flex;
  justify-content: center;
  font-size: ${props => `${props.fontSizeTile}px`};
  font-weight: bold;

`;

const FooterDOM = styled.div<{ 
  footerHeight: number 
}>`
  display: flex;
  justify-content: center;
  height: ${props => `${props.footerHeight}px`};
`;

export default Node;