import React from 'react';
import { useDispatch } from 'react-redux';
import { addNode } from '../../redux/slices/nodeSlice';
import './AddNodesButton.css'

const node1 = {
  id: "node1",
  name:"Node 1",
  nodetype: "type1",
  x: 300,
  y: 200,
  parent: "b",
  inputs: [
    { 
      portId: "port1", 
      name:"Port 1",
      type: "input",
      properties: {},
      x: 0,
      y: 0,
    },
    { 
      portId: "port1b", 
      name:"Port 2",
      type: "input",
      properties: {},
      x: 0,
      y: 0,
    }
  ],
  outputs: [
    { 
      portId: "port2", 
      name:"Port 2",
      type: "output",
      properties: {},
      x: 0,
      y: 0,
    }
  ]
};

const node2 = {
  id: "node2",
  name:"Node 2",
  nodetype: "type2",
  x: 300,
  y: 400,
  parent: "b",
  inputs: [
    { 
      portId: "port3", 
      name:"Port 3",
      type: "input",
      properties: {},
      x: 0,
      y: 0,
    },
    { 
      portId: "port5", 
      name:"Port 5",
      type: "input",
      properties: {},
      x: 0,
      y: 0,
    }
  ],
  outputs: [
    { 
      portId: "port4", 
      name:"Port 4",
      type: "output",
      properties: {},
      x: 0,
      y: 0,
    }
  ]
};

const AddNodesButton: React.FC = () => {
  const dispatch = useDispatch();

  const handleClick = () => {
    dispatch(addNode(node1));
    dispatch(addNode(node2));
  };

  return (
    <button className="button" onClick={handleClick}>Add Nodes</button>
  );
};

export default AddNodesButton;