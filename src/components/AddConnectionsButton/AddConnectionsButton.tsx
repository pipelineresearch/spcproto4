// AddConnectionsButton.tsx
import React from 'react';
import { useDispatch } from 'react-redux';
import { addConnection } from '../../redux/slices/connectionSlice';
import './AddConnectionsButton.css'

const AddConnectionsButton: React.FC = () => {
  const dispatch = useDispatch();
  
  const handleClick = () => {
    dispatch(addConnection({
      id: 'conn_' + Date.now(),
      source: { nodeId: "node1", outputPortId: "port2" },
      target: { nodeId: "node2", inputPortId: "port5" },
    }));
  };

  return (
    <button className="button" onClick={handleClick}>Add Connection</button>
  );
};

export default AddConnectionsButton;