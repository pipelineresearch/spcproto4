import React from 'react';
import { useDispatch } from 'react-redux';
import { useDrop} from "react-dnd";
import { v4 as uuidv4 } from "uuid";
import { styled } from '@linaria/react';
import { addConnection } from '../../redux/slices/connectionSlice';
import { nodeConfig } from '../Node/Node.config';
import {Connection as connectionTypes} from '../../types';

interface InputPortProps {
  nodeId: string;
  portId: string;
  portName: string;
  x: number;
  y: number;
  parentId: string;
  nodeConfig: typeof nodeConfig;
}

const ItemTypes = {
    PORT: "port",
  };

const InputWidgetPort: React.FC<InputPortProps> = ({ nodeId, portId, portName, x, y, parentId, nodeConfig }) => {
  const dispatch = useDispatch();
  
  const [, drop] = useDrop(() => ({
    accept: ItemTypes.PORT,
    drop: (item:connectionTypes , monitor) => {
      console.log(item);
      const didDrop = monitor.didDrop();
      if (didDrop) {
        return;
      }

      dispatch(
        addConnection({
          id: uuidv4(),
          parentId: nodeId,
          source: {
            nodeId: item.source.nodeId,
            outputPortId: item.source.outputPortId,
          },
          target: {
            nodeId: nodeId,
            inputPortId: portId,
          },
        })
      );
    },
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  }));


  // console.log({nodeId , portId , portName , x , y , parentId , nodeConfig});
  return (
    <div ref={drop} style={{position:"absolute",right:x+"px", top:y+"px", display:"flex", alignItems:"center" , backgroundColor:"gray", borderRadius:"30px 0 0 30px", padding:"5px"}}>
      <div style={{height:"13px", width:"13px", borderRadius:"50%", backgroundColor:"green"}}/>
      <div style={{margin:"0 10px 0 10px"}}>{portName}</div>
    </div>
  );
};

export default InputWidgetPort;
