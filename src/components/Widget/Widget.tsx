import React, { Suspense , useRef} from 'react';
import { Widget as WidgetType } from "../../types"; 
import { useDispatch } from 'react-redux';
import { toggleWidget } from '../../redux/slices/uiSlice';
import icon1 from './mechanism.png';

interface WidgetProps {
  widget: WidgetType;
}

const componentMap = {
    "NodeArea": React.lazy(() => import('../../components/NodeArea')),
    "LibraryNodes": React.lazy(() => import('../../containers/LibraryNodes')),
    "BarChart": React.lazy(() => import('../../components/BarChart')),
};

const Widget: React.FC<WidgetProps> = ({ widget }) => {
  
  const dispatch = useDispatch();
  const isNodeAreaActive = widget.isNodeAreaActive
  const Component = componentMap[isNodeAreaActive ? "NodeArea" : widget.component];
  const WidgetRef = useRef<HTMLDivElement>(null);

  const toogleWidgetHandle = (event) => {
    event.preventDefault();
    dispatch(toggleWidget(widget.id))
  }

  
  return Component ? (
    <div ref={WidgetRef} key={widget.id} data-grid={{ isResizable: true }} style={{width:"100%", height:"100%"}}>
      <div style={{height:"calc(100% - 30px)"}}>
        <div className="dragHandle">{widget.name}
        <span style={{float:"right"}} onClick={event => toogleWidgetHandle(event)}><img src={icon1} alt="icon1" style={{width:"25px"}}/></span> {/* X icon */}
        </div>
        <Suspense fallback={<div>Loading...</div>}>
          <Component  key={widget.id} id={widget.id} parentUI={widget} x={WidgetRef.current?.getBoundingClientRect().x} y={WidgetRef.current?.getBoundingClientRect().y}/>
        </Suspense>
      </div>
    </div>
  ) : (
    <div style={{height:"calc(100% - 30px)"}}>
        <div className="dragHandle">{widget.name}
        <span style={{float:"right"}} onClick={event => toogleWidgetHandle(event)}><img src={icon1} alt="icon1" style={{width:"25px"}}/></span> {/* X icon */}
      <div>Au boulot , le composant {widget.component} ne va pas se faire tout seul ! </div>
    </div>
    </div>
  );
};

export default Widget;