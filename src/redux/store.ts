// src/redux/store.ts
import { configureStore } from '@reduxjs/toolkit';
import nodesReducer from './slices/nodeSlice';
import connectionsReducer from './slices/connectionSlice';
import uiReducer from './slices/uiSlice';
import cardReducer from './slices/cardSlice'

const rootReducer = {
  cards:cardReducer,
  nodes: nodesReducer,
  connections: connectionsReducer,
  ui: uiReducer,
};

const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;