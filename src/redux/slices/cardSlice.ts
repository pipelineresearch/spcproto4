// src/redux/nodesSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Node } from '../../types';

// Define the initial state
const initialState: Record<string, Node> = {};

// Define the slice
const nodeSlice = createSlice({
  name: 'nodes',
  initialState,
  reducers: {
    addCard: (state, action: PayloadAction<Node>) => {
      const node = action.payload;
      
      state[node.id] = node;
    },
    selectCard: (state, action: PayloadAction<string>) => {
      const nodeId = action.payload;
      for (const id in state) {
        state[id].selected = id === nodeId;
      }
    },
    removeAllCards: (state, action: PayloadAction<Node[]>) => {
      const nodes = action.payload;
      console.log(nodes)
      Object.values(nodes).map(node => state[node.id] = null)
    }
    
  },
});

// Export the actions
export const { addCard, selectCard, removeAllCards } = nodeSlice.actions;

// Export the reducer
export default nodeSlice.reducer;