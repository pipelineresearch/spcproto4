// src/redux/nodesSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Node } from '../../types';
import { nodeConfig } from '../../components/Node/Node.config';

// Define the initial state
const initialState: Record<string, Node> = {};

// Define the slice
const nodeSlice = createSlice({
  name: 'nodes',
  initialState,
  reducers: {
    addNode: (state, action: PayloadAction<Node>) => {
      const node = action.payload;
      const newNode: Node = {
        ...node,
        inputs: node.inputs.map((input, index) => ({
          ...input,
          x: nodeConfig.inputsMarginLeft,
          y: nodeConfig.headerHeight + index * nodeConfig.spacingVerticalBetweenPorts,
        })),
        outputs: node.outputs.map((output, index) => ({
          ...output,
          x: nodeConfig.nodeWidth + nodeConfig.outputsMarginRight,
          y: nodeConfig.headerHeight + index * nodeConfig.spacingVerticalBetweenPorts,
        })),
      };
      state[node.id] = newNode;
    },
    updateNode: (state, action: PayloadAction<Node>) => {
      const node = action.payload;
      state[node.id] = node;
    },
    moveNode: (state, action: PayloadAction<{ nodeId: string, x: number, y: number }>) => {
      const { nodeId, x, y } = action.payload;
      state[nodeId].x = x;
      state[nodeId].y = y;
    },
    deleteNode: (state, action: PayloadAction<string>) => {
      const nodeId = action.payload;
      delete state[nodeId];
    },
    selectNode: (state, action: PayloadAction<string>) => {
      const nodeId = action.payload;
      for (const id in state) {
        state[id].selected = id === nodeId;
      }
    },

    
  },
});

// Export the actions
export const { addNode, updateNode, moveNode, deleteNode, selectNode } = nodeSlice.actions;

// Export the reducer
export default nodeSlice.reducer;