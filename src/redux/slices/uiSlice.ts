// uiSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UIState, Widget } from '../../types/uiTypes'; // Assurez-vous d'importer le type MosaicWindow ici

const initialState: UIState = {
  selectedNodeId: null,
  widgets: [
    { id: 'a', name: 'Nodes Library', component: 'LibraryNodes' , category:"widget", x: 0, y: 0, w: 17, h:75, inputs:{}, isNodeAreaActive:false},
    { id: 'b', name: "Karlab asset conformation", component: 'Logger', category:"widget", x: 17, y: 0, w: 83, h: 47 , 
    inputs:[{
      portId:"logger",
      name:"logger",
      type:"stream",
      properties:{},
      x:0,
      y:50,
      data:[]
    }], isNodeAreaActive:true},
    // { id: 'c', name: 'Widget BarChart', component: 'BarChart' , x: 15, y: 15, w: 20, h: 28},
  ],
};

const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    selectNode: (state, action: PayloadAction<string>) => {
      state.selectedNodeId = action.payload;
    },
    deselectNode: (state) => {
      state.selectedNodeId = null;
    },
    // Nouvelle action pour créer une fenêtre
    addWidget: (state, action: PayloadAction<Widget>) => {
      state.widgets.push(action.payload);
    },
    toggleWidget: (state, action: PayloadAction<string>) => {
      const widget = state.widgets.find(widget => widget.id === action.payload);
      if (widget) {
        widget.isNodeAreaActive =  !widget.isNodeAreaActive;
      }
    },
  },
});

export const { selectNode, deselectNode, addWidget, toggleWidget } = uiSlice.actions;

export default uiSlice.reducer;