// connectionSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Connection, Connections } from '../../types/connectionTypes';

const initialState: Connections = {};

const connectionsSlice = createSlice({
  name: 'connections',
  initialState,
  reducers: {
    addConnection: (state, action: PayloadAction<Connection>) => {
      state[action.payload.id] = action.payload;
    },
    removeConnection: (state, action: PayloadAction<string>) => {
      delete state[action.payload];
    },
    updateConnection: (state, action: PayloadAction<{id: string, changes: Partial<Connection>}>) => {
      const { id, changes } = action.payload;
      if (state[id]) {
        state[id] = { ...state[id], ...changes };
      }
    },
  },
});

export const { addConnection, removeConnection, updateConnection } = connectionsSlice.actions;

export default connectionsSlice.reducer;