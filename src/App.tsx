import React from "react";
import { DndProvider , useDrop} from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import "./App.css";
import WidgetsArea from "./components/WidgetsArea";

const App: React.FC = () => {

  return (
    <DndProvider backend={HTML5Backend}>
      <WidgetsArea />
    </DndProvider>
  );
};

export default App;