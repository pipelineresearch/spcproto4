// LibraryNodes.tsx

import React , {useMemo} from 'react';
import { styled } from '@linaria/react';
import {Node as NodeType} from '../../types'
import { useDispatch, useSelector } from 'react-redux';
import { addCard, removeAllCards } from '../../redux/slices/cardSlice';
import NodeCard from '../../components/NodeCard';
import { Widget as widgetTypes } from '../../types';
import e from 'express';

type NodeStoreType = {
    content: NodeType;
    extension: string;
    name: string;
    type: string;
}

type stateCards = {
    cards: NodeType;
    ui: stateWidgets;
}
type stateWidgets = {
    selectedNodeID: string | null;
    widgets: widgetTypes[];
}


const LibraryNodes: React.FC = () => {
    const dispatch = useDispatch();
    const nodeInStore = useSelector((state: stateCards) => state.cards);
    const widgetsInStore = useSelector((state:stateCards) => state.ui.widgets);
    const [loaded, setloaded] = React.useState(false);

    const loadExternalCards = (event) => {
        // event.preventDefault();
        if(loaded) return;
        if(Object.keys(nodeInStore).length > 0){
            dispatch(removeAllCards(nodeInStore));
            setloaded(false);
        }
        
        fetch('http://localhost:3001/files')
        .then(response => response.json())
        .then((files:object) => Object.values(files).map((file:NodeStoreType) => {
            file.content.parent = "library";
            dispatch(addCard(file.content))
            setloaded(true);
        }));

    }


    
    // React.useEffect(() => {
    //     loadExternalCards()
    // }, [widgetsInStore.length])

      console.log("reloaded")
    return (
        <LibraryNodesDOM>
                <LoadButtonDOM onClick={event => loadExternalCards(event)}>Load Nodes</LoadButtonDOM>

                {
                   nodeInStore && Object.values(nodeInStore).map((node:NodeType, index:number) => {
                        if(node && node.parent === "library") {
                            return (
                                <NodeCard 
                                    key={index} 
                                    {...node} 
                                />
                            )
                        }
                    })
                }
        </LibraryNodesDOM>
    );
    }

export default LibraryNodes

const LoadButtonDOM = styled.div` 
    display: inline-block;
    padding: 10px 20px;
    margin: 20px;
    font-size: 16px;
    color: #ffffff;
    background-color: #4a4a4a;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    text-align: center;
    transition: background-color 0.3s ease;
  `

const LibraryNodesDOM = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    background-color: #222;
    `
