// uiTypes.ts
export interface UIState {
  selectedNodeId: string | null;
  widgets: Widget[]; // Ajoutez l'état des fenêtres Mosaic ici
}


export interface Widget {
  id: string;
  name: string;
  component: string; // Le nom du composant à afficher dans cette fenêtre.
  isNodeAreaActive: boolean; 
  category: string;
  x: number;
  y: number;
  w: number;
  h: number;
  inputs: object
}