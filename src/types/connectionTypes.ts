// connectionTypes.ts

export interface Connection {
  id: string;
  parentId: string;
  source: {
    nodeId: string;
    outputPortId: string;
  };
  target: {
    nodeId: string;
    inputPortId: string;
  };
}

export interface Connections {
  [id: string]: Connection;
}