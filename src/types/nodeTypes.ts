
export interface Port {
  name:string;
  portId: string;
  type: string;
  properties: object;
  x: number; // Add this
  y: number; // Add this
}

export interface Node {
  id: string;
  name:string;
  category: string;
  x?: number;
  y?: number;
  inputs: Port[];
  outputs: Port[];
  parent?: string;
  selected?: boolean;
}

export interface NodeState {
  [id: string]: Node;
}

